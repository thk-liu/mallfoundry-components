import { ReactChildren } from "react"
import "taro-ui/dist/style/index.scss"
import "./app.scss"

interface AppProps {
  children: ReactChildren
}

export default function App(props: AppProps) {
  const { children } = props
  return (
    children
  )
}
