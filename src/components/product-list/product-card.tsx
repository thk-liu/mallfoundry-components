import * as classNames from "classnames"
import * as React from "react"
import { useState } from "react"
import { Image, View } from "@tarojs/components"
import "./product-card.scss"
import ProductPrice from "../product-price"

interface ProductCardImageProps {
  src?: string
}

function ProductCardImage(props: ProductCardImageProps) {
  const { src } = props
  const [loading, setLoading] = useState(true)
  return (
    <Image
      lazyLoad
      mode="widthFix"
      className={classNames("product-card-image", {
        "product-card-image-loading": loading,
      })}
      src={src}
      onLoad={() => setLoading(false)}
    />
  )
}

interface ProductCardProps {
  id: string
  title: string
  imageUrl: string
}

export default function ProductCard(props: ProductCardProps) {
  const { imageUrl, title } = props
  return (
    <View className="product-card">
      <ProductCardImage src={imageUrl} />
      <View className="product-card-content">
        <View className="product-card-title">{title}</View>
        <ProductPrice amount={10.12} />
      </View>
    </View>
  )
}
