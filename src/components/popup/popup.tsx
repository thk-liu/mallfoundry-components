import * as React from "react"
import { ReactNode } from "react"
import * as classNames from "classnames"
import { View } from "@tarojs/components"
import { AtIcon } from "taro-ui"
import { prefixClassName } from "../../styles"
import "./popup.scss"
import Mask from "../mask"

interface PopupHeaderProps {
  closeable?: boolean;
  children?: ReactNode
  onClose?: () => void
}

function PopupHeader({ closeable, children, onClose }: PopupHeaderProps) {
  return (
    <View className={prefixClassName("popup-header")}>
      {children}
      {closeable && <AtIcon className={prefixClassName("popup-close")} value="close" onClick={onClose} />}
    </View>
  )
}

interface PopupContainerProps {
  closeable?: boolean;
  children?: ReactNode;
  onClose?: () => void
}

function PopupContainer({ closeable, children, onClose }: PopupContainerProps) {
  return (
    <View className={prefixClassName("popup-container")}>
      <PopupHeader closeable={closeable} onClose={onClose} />
      {children}
    </View>
  )
}

interface PopupProps {
  opened?: boolean
  closeable?: boolean
  children?: ReactNode
  onClose?: () => void
}

export default function Popup(props: PopupProps) {
  const { opened, closeable, children, onClose } = props

  return (
    <View className={classNames(prefixClassName("popup"),
      {
        [prefixClassName("active")]: opened,
      })}
    >
      <Mask visible={opened} closable onClose={onClose} />
      <PopupContainer closeable={closeable} onClose={onClose}>
        {children}
      </PopupContainer>
    </View>
  )
}
