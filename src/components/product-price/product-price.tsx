import * as React from "react"
import { View } from "@tarojs/components"
import "./product-price.scss"

interface ProductPriceProps {
  currency?: string
  amount: number
}

export default function ProductPrice(props: ProductPriceProps) {
  const { currency = "¥", amount } = props
  return (
    <View className="product-price">
      <View className="product-price-currency">{currency}</View>
      {amount}
    </View>
  )
}
